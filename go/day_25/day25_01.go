package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type node struct {
	val       int
	neighbors map[direction]*node
}

type action struct {
	output int
	next   string
	move   direction
}

type state struct {
	name    string
	mapping map[int]*action
}

type direction int

const (
	left direction = iota
	right
)

var strToDir = map[string]direction{"left": left, "right": right}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	states := map[string]*state{}
	start := ""
	steps := 0

	scanner.Scan()
	fmt.Sscanf(scanner.Text(), "Begin in state %s.", &start)
	start = strings.TrimSuffix(start, ".")
	scanner.Scan()
	fmt.Sscanf(scanner.Text(), "Perform a diagnostic checksum after %d steps.", &steps)

	for scanner.Scan() {
		currentState := ""
		currentValue := -1
		currentMove := ""
		nextValue := -1
		nextState := ""

		scanner.Text()
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "In state %s:", &currentState)
		currentState = strings.TrimSuffix(currentState, ":")
		st := &state{name: currentState}
		st.mapping = map[int]*action{}

		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "  If the current value is %d:", &currentValue)
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "    - Write the value %d.", &nextValue)
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "    - Move one slot to the %s.", &currentMove)
		currentMove = strings.TrimSuffix(currentMove, ".")
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "    - Continue with state %s.", &nextState)
		nextState = strings.TrimSuffix(nextState, ".")
		act := &action{nextValue, nextState, strToDir[currentMove]}
		st.mapping[currentValue] = act

		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "  If the current value is %d:", &currentValue)
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "    - Write the value %d.", &nextValue)
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "    - Move one slot to the %s.", &currentMove)
		currentMove = strings.TrimSuffix(currentMove, ".")
		scanner.Scan()
		fmt.Sscanf(scanner.Text(), "    - Continue with state %s.", &nextState)
		nextState = strings.TrimSuffix(nextState, ".")
		act = &action{nextValue, nextState, strToDir[currentMove]}
		st.mapping[currentValue] = act

		states[currentState] = st
	}

	currentState := states[start]
	currentNode := &node{neighbors: map[direction]*node{}}

	for i := 0; i < steps; i++ {
		val := currentNode.val
		act := currentState.mapping[val]
		currentNode.val = act.output
		currentState = states[act.next]
		if currentNode.neighbors[act.move] == nil {
			next := &node{neighbors: map[direction]*node{}}
			currentNode.neighbors[act.move] = next
			next.neighbors[(act.move+1)%2] = currentNode
		}
		currentNode = currentNode.neighbors[act.move]
	}

	checksum := currentNode.val
	ln := currentNode.neighbors[left]
	for ln != nil {
		checksum += ln.val
		ln = ln.neighbors[left]
	}
	rn := currentNode.neighbors[right]
	for rn != nil {
		checksum += rn.val
		rn = rn.neighbors[right]
	}

	fmt.Println(checksum)
}
