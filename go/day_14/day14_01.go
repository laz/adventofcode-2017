package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	input := ""
	if len(os.Args) > 1 {
		input = os.Args[1]
	} else {
		r := os.Stdin
		scanner := bufio.NewScanner(r)
		scanner.Scan()
		input = scanner.Text()
	}

	count := 0
	for i := 0; i < 128; i++ {
		hex := knotHash(input + "-" + strconv.Itoa(i))
		bin := ""
		for _, r := range hex {
			n, _ := strconv.ParseInt(string(r), 16, 64)
			bin += fmt.Sprintf("%04b", n)
		}
		count += strings.Count(bin, "1")
	}
	fmt.Println(count)
}

func knotHash(input string) string {
	lengths := make([]int, len(input)+5)

	for i, r := range input {
		lengths[i] = int(r)
	}

	suffix := []int{17, 31, 73, 47, 23}
	for i, j := range suffix {
		lengths[len(input)+i] = j
	}

	size := 256
	list := make([]int, size)
	for i := range list {
		list[i] = i
	}

	pos := 0
	skip := 0
	rounds := 0

	for rounds < 64 {
		for _, l := range lengths {
			sect := make([]int, l)
			for i := range sect {
				sect[l-(i+1)] = list[(pos+i+size)%size]
			}
			for i, c := range sect {
				list[(pos+i+size)%size] = c
			}
			pos = (pos + l + skip + size) % size
			skip++
		}
		rounds++
	}

	dense := make([]byte, size/16)
	for i := range dense {
		val := list[i*16]
		for j := i*16 + 1; j < i*16+16; j++ {
			val ^= list[j]
		}
		dense[i] = byte(val)
	}

	return fmt.Sprintf("%x", dense)
}
