package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type point struct {
	x    int
	y    int
	used bool
}

type grid [][]point

func main() {
	input := ""
	if len(os.Args) > 1 {
		input = os.Args[1]
	} else {
		r := os.Stdin
		scanner := bufio.NewScanner(r)
		scanner.Scan()
		input = scanner.Text()
	}

	g := grid(make([][]point, 128))
	for i := 0; i < 128; i++ {
		hex := knotHash(input + "-" + strconv.Itoa(i))
		bin := ""
		for _, r := range hex {
			n, _ := strconv.ParseInt(string(r), 16, 64)
			bin += fmt.Sprintf("%04b", n)
		}
		row := make([]point, 128)
		for j, r := range bin {
			row[j] = point{j, i, r == '1'}
		}
		g[i] = row
	}

	groups := 0
	seen := map[point]bool{}

	for _, row := range g {
		for _, space := range row {
			if !seen[space] {
				seen[space] = true
				if space.used {
					groups++
					used := []point{space}
					for len(used) > 0 {
						more := []point{}
						for _, next := range used {
							for _, adjacent := range getAdjacents(next, g) {
								if !seen[adjacent] {
									seen[adjacent] = true
									if adjacent.used {
										more = append(more, adjacent)
									}
								}
							}
						}
						used = more
					}
				}
			}
		}
	}
	fmt.Println(groups)
}

func getAdjacents(space point, g grid) []point {
	adjacent := []point{}
	if space.y-1 >= 0 {
		adjacent = append(adjacent, g[space.y-1][space.x])
	}
	if space.y+1 < 128 {
		adjacent = append(adjacent, g[space.y+1][space.x])
	}
	if space.x-1 >= 0 {
		adjacent = append(adjacent, g[space.y][space.x-1])
	}
	if space.x+1 < 128 {
		adjacent = append(adjacent, g[space.y][space.x+1])
	}
	return adjacent
}

func knotHash(input string) string {
	lengths := make([]int, len(input)+5)

	for i, r := range input {
		lengths[i] = int(r)
	}

	suffix := []int{17, 31, 73, 47, 23}
	for i, j := range suffix {
		lengths[len(input)+i] = j
	}

	size := 256
	list := make([]int, size)
	for i := range list {
		list[i] = i
	}

	pos := 0
	skip := 0
	rounds := 0

	for rounds < 64 {
		for _, l := range lengths {
			sect := make([]int, l)
			for i := range sect {
				sect[l-(i+1)] = list[(pos+i+size)%size]
			}
			for i, c := range sect {
				list[(pos+i+size)%size] = c
			}
			pos = (pos + l + skip + size) % size
			skip++
		}
		rounds++
	}

	dense := make([]byte, size/16)
	for i := range dense {
		val := list[i*16]
		for j := i*16 + 1; j < i*16+16; j++ {
			val ^= list[j]
		}
		dense[i] = byte(val)
	}

	return fmt.Sprintf("%x", dense)
}
