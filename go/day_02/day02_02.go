package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	sum := 0
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), "\t")
		cells := make([]int, len(line))
		for i, r := range line {
			n, _ := strconv.Atoi(r)
			cells[i] = n
		}
		sort.Ints(cells)
		for i := len(cells) - 1; i > 0; i-- {
			for j := 0; j < i; j++ {
				if cells[i]%cells[j] == 0 {
					sum += cells[i] / cells[j]
					i = 0
				}
			}
		}
	}
	fmt.Println(sum)
}
