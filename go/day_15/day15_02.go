package main

import "fmt"

var mod = 2147483647

type generator struct {
	value  int
	factor int
	gate   int
}

func (g *generator) next() int {
	g.value = (g.value * g.factor) % mod
	for g.value%g.gate != 0 {
		g.value = (g.value * g.factor) % mod
	}
	return g.value & 0xffff
}

func main() {
	a := &generator{634, 16807, 4}
	b := &generator{301, 48271, 8}
	rounds := 5000000

	count := 0
	for i := 0; i < rounds; i++ {
		if a.next() == b.next() {
			count++
		}
	}

	fmt.Println(count)
}
