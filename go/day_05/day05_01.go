package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	jumps := make([]int, 0, 1100)
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		input := scanner.Text()
		j, _ := strconv.Atoi(input)
		jumps = append(jumps, j)
	}

	count := 0
	current := 0
	for current < len(jumps) {
		jump := jumps[current]
		jumps[current]++
		current += jump
		count++
	}

	fmt.Println(count)
}
