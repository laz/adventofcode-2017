package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	connections := map[string][]string{}
	for scanner.Scan() {
		input := strings.Split(scanner.Text(), " <-> ")
		program := input[0]
		linked := strings.Split(input[1], ", ")
		connections[program] = linked
	}

	count := 1
	domain := []string{"0"}
	seen := map[string]bool{"0": true}

	for len(domain) > 0 {
		next := []string{}
		for _, cur := range domain {
			test := connections[cur]
			for _, t := range test {
				if !seen[t] {
					count++
					seen[t] = true
					next = append(next, t)
				}
			}
		}
		domain = next
	}

	fmt.Println(count)
}
