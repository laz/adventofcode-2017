package main

import (
	"bufio"
	"fmt"
	"os"
)

type point struct {
	x, y int
}

func (p point) move(d int) point {
	m := moves[d]
	return point{p.x + m.x, p.y + m.y}
}

var moves = []point{{0, -1}, {1, 0}, {0, 1}, {-1, 0}}

func turn(cur int, dir int) int {
	a := -1
	if dir == right {
		a = 1
	}
	return (cur + a + 4) % 4
}

func opposite(dir int) int {
	return (dir + 2) % 4
}

const (
	up = iota
	right
	down
	left
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	points := map[point]bool{}
	input := make([]string, 0, 1000)

	for i := 0; scanner.Scan(); i++ {
		line := scanner.Text()
		input = append(input, line)
	}

	mid := (len(input) - 1) / 2
	for y, row := range input {
		for x, p := range row {
			points[point{x - mid, y - mid}] = p == '#'
		}
	}

	current := point{0, 0}
	direction := up
	bursts := 10000
	infected := 0

	for i := 0; i < bursts; i++ {
		if points[current] {
			direction = turn(direction, right)
			points[current] = false
		} else {
			direction = turn(direction, left)
			points[current] = true
			infected++
		}
		current = current.move(direction)
	}

	fmt.Println(infected)
}
