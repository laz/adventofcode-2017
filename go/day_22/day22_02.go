package main

import (
	"bufio"
	"fmt"
	"os"
)

type point struct {
	x, y int
}

func (p point) move(d direction) point {
	m := moves[d]
	return point{p.x + m.x, p.y + m.y}
}

var moves = []point{{0, -1}, {1, 0}, {0, 1}, {-1, 0}}

func turn(cur direction, dir direction) direction {
	a := -1
	if dir == right {
		a = 1
	}
	return direction((int(cur) + a + 4) % 4)
}

func opposite(dir direction) direction {
	return direction((int(dir) + 2) % 4)
}

type direction int

const (
	up direction = iota
	right
	down
	left
)

type state int

const (
	clean state = iota
	weakened
	infected
	flagged
)

func transition(s state) state {
	return state((int(s) + 1) % 4)
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	points := map[point]state{}
	input := make([]string, 0, 1000)

	for i := 0; scanner.Scan(); i++ {
		line := scanner.Text()
		input = append(input, line)
	}

	mid := (len(input) - 1) / 2
	for y, row := range input {
		for x, p := range row {
			cur := point{x - mid, y - mid}
			if p == '#' {
				points[cur] = infected
			} else {
				points[cur] = clean
			}
		}
	}

	current := point{0, 0}
	direction := up
	bursts := 10000000
	infections := 0

	for i := 0; i < bursts; i++ {
		switch points[current] {
		case clean:
			direction = turn(direction, left)
		case weakened:
			infections++
		case infected:
			direction = turn(direction, right)
		case flagged:
			direction = opposite(direction)
		}
		points[current] = transition(points[current])
		current = current.move(direction)
	}

	fmt.Println(infections)
}
