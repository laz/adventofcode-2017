package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type layer struct {
	depth  int
	wrange int
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	firewall := make([]layer, 0, 100)

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		input := strings.Split(scanner.Text(), ": ")
		dep, _ := strconv.Atoi(input[0])
		ran, _ := strconv.Atoi(input[1])
		firewall = append(firewall, layer{dep, ran})
	}

	severity := 0
	for _, l := range firewall {
		if l.depth%((l.wrange-1)*2) == 0 {
			severity += l.depth * l.wrange
		}
	}

	fmt.Println(severity)
}
