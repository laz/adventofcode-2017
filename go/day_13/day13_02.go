package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type layer struct {
	depth  int
	wrange int
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	firewall := make([]layer, 0, 100)

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		input := strings.Split(scanner.Text(), ": ")
		dep, _ := strconv.Atoi(input[0])
		ran, _ := strconv.Atoi(input[1])
		firewall = append(firewall, layer{dep, ran})
	}

	collided := true
	start := 0
	for collided {
		collided = false
		for _, l := range firewall {
			if (l.depth+start)%((l.wrange-1)*2) == 0 {
				collided = true
				start++
				break
			}
		}
	}

	fmt.Println(start)
}
