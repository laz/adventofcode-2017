package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type grid [][]byte

type splitGrid [][]grid

var initialGrid = []string{".#.", "..#", "###"}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	rules := map[string]string{}
	for scanner.Scan() {
		rule := strings.Split(scanner.Text(), " => ")
		rules[rule[0]] = rule[1]
	}

	pixels := grid(make([][]byte, len(initialGrid), 100))

	for i, row := range initialGrid {
		pixels[i] = []byte(row)
	}

	iterations := 5
	for i := 0; i < iterations; i++ {
		splitPixels := split(pixels)
		for j, row := range splitPixels {
			for k, col := range row {
				r := matchingRule(col, rules)
				trans := apply(col, r)
				splitPixels[j][k] = trans
			}
		}
		pixels = combine(splitPixels)
	}

	count := 0
	for _, row := range pixels {
		for _, col := range row {
			if col == '#' {
				count++
			}
		}
	}

	fmt.Println(count)
}

func split(pixels grid) splitGrid {
	size := len(pixels)
	splitSize := 2
	if size%2 != 0 && size%3 == 0 {
		splitSize = 3
	}
	squares := size / splitSize
	results := splitGrid(make([][]grid, squares))

	for i := 0; i < squares; i++ {
		results[i] = (make([]grid, squares))
		for j := 0; j < squares; j++ {
			results[i][j] = grid(make([][]byte, splitSize))
			for k := 0; k < splitSize; k++ {
				results[i][j][k] = make([]byte, splitSize)
			}
		}
	}

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			results[i/splitSize][j/splitSize][i%splitSize][j%splitSize] = pixels[i][j]
		}
	}
	return results
}

func matchingRule(pixels grid, rules map[string]string) string {
	size := len(pixels)
	test := make([]string, size)

	for i, row := range pixels {
		test[i] = string(row)
	}

	result := rules[strings.Join(test, "/")]

	if result == "" {
		for i := range pixels {
			test[i] = string(pixels[size-1-i])
		}
		result = rules[strings.Join(test, "/")]
	}

	if result == "" {
		for i, row := range pixels {
			test[i] = string(reverse(row))
		}
		result = rules[strings.Join(test, "/")]
	}

	if result == "" {
		for i := range pixels {
			test[i] = string(reverse(pixels[size-1-i]))
		}
		result = rules[strings.Join(test, "/")]
	}

	if result == "" {
		for i := 0; i < size; i++ {
			b := make([]byte, size)
			for j := 0; j < size; j++ {
				b[j] = pixels[j][i]
			}
			test[i] = string(b)
		}
		result = rules[strings.Join(test, "/")]
	}

	if result == "" {
		for i := 0; i < size; i++ {
			b := make([]byte, size)
			for j := 0; j < size; j++ {
				b[j] = pixels[j][i]
			}
			test[i] = string(reverse(b))
		}
		result = rules[strings.Join(test, "/")]
	}

	if result == "" {
		for i := size - 1; i >= 0; i-- {
			b := make([]byte, size)
			for j := 0; j < size; j++ {
				b[j] = pixels[j][i]
			}
			test[size-1-i] = string(b)
		}
		result = rules[strings.Join(test, "/")]
	}

	if result == "" {
		for i := size - 1; i >= 0; i-- {
			b := make([]byte, size)
			for j := 0; j < size; j++ {
				b[j] = pixels[j][i]
			}
			test[size-1-i] = string(reverse(b))
		}
		result = rules[strings.Join(test, "/")]
	}

	return result
}

func reverse(s []byte) []byte {
	r := make([]byte, len(s))
	for i, b := range s {
		r[len(r)-1-i] = b
	}
	return r
}

func apply(pixels grid, r string) grid {
	trans := strings.Split(r, "/")
	result := grid(make([][]byte, len(trans)))
	for i, s := range trans {
		result[i] = []byte(s)
	}
	return result
}

func combine(split splitGrid) grid {
	splitSize := len(split[0][0])
	size := len(split) * splitSize

	result := grid(make([][]byte, size))
	for i := 0; i < size; i++ {
		result[i] = make([]byte, size)
	}

	for i, outer := range split {
		for j, g := range outer {
			for k, row := range g {
				for l, col := range row {
					//fmt.Printf("[%d][%d] = [%d][%d][%d][%d] %d %d %d\n", (i*splitSize + k), (j*splitSize + l), i, j, k, l, splitSize, len(split), size)
					result[i*splitSize+k][j*splitSize+l] = col
				}
			}
		}
	}
	return result
}
