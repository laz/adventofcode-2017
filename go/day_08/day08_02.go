package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	registers := map[string]int{}
	max := 0
	for scanner.Scan() {
		input := strings.Split(scanner.Text(), " ")
		cmd := input[:3]
		cond := input[len(input)-3:]

		passed := false
		test, _ := strconv.Atoi(cond[2])
		switch cond[1] {
		case ">":
			passed = registers[cond[0]] > test
		case ">=":
			passed = registers[cond[0]] >= test
		case "<":
			passed = registers[cond[0]] < test
		case "<=":
			passed = registers[cond[0]] <= test
		case "==":
			passed = registers[cond[0]] == test
		case "!=":
			passed = registers[cond[0]] != test
		}

		if passed {
			val, _ := strconv.Atoi(cmd[2])
			switch cmd[1] {
			case "inc":
				registers[cmd[0]] += val
			case "dec":
				registers[cmd[0]] -= val
			}
			reg := registers[cmd[0]]
			if reg > max {
				max = reg
			}
		}
	}
	fmt.Println(max)

	max = 0
	for _, v := range registers {
		if v > max {
			max = v
		}
	}

	fmt.Println(max)
}
