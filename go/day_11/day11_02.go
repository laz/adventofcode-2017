package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type direction int

const (
	n direction = iota
	ne
	se
	s
	sw
	nw
)

var directions = map[string]direction{
	"n":  n,
	"ne": ne,
	"se": se,
	"s":  s,
	"sw": sw,
	"nw": nw,
}

type hexTile struct {
	x int
	y int
	z int
}

func move(h hexTile, d direction) hexTile {
	switch d {
	case n:
		return hexTile{h.x, h.y + 1, h.z - 1}
	case ne:
		return hexTile{h.x + 1, h.y, h.z - 1}
	case se:
		return hexTile{h.x + 1, h.y - 1, h.z}
	case s:
		return hexTile{h.x, h.y - 1, h.z + 1}
	case sw:
		return hexTile{h.x - 1, h.y, h.z + 1}
	case nw:
		return hexTile{h.x - 1, h.y + 1, h.z}
	}
	return h
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	scanner.Scan()
	input := strings.Split(scanner.Text(), ",")

	start := hexTile{0, 0, 0}
	curr := start
	max := 0

	for _, s := range input {
		dir := directions[s]
		next := move(curr, dir)
		curr = next
		steps := calcSteps(curr)
		if steps > max {
			max = steps
		}
	}

	fmt.Println(max)
}

func calcSteps(h hexTile) int {
	steps := 0
	if h.x >= 0 {
		steps += h.x
	}
	if h.y >= 0 {
		steps += h.y
	}
	if h.z >= 0 {
		steps += h.z
	}
	return steps
}
