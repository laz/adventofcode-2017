package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	scanner.Scan()
	input := strings.Split(scanner.Text(), ",")

	lengths := make([]int, len(input))

	for i, s := range input {
		j, _ := strconv.Atoi(s)
		lengths[i] = j
	}

	size := 256
	list := make([]int, size)
	for i := range list {
		list[i] = i
	}

	pos := 0
	skip := 0
	for _, l := range lengths {
		sect := make([]int, l)
		for i := range sect {
			sect[l-(i+1)] = list[(pos+i+size)%size]
		}
		for i, c := range sect {
			list[(pos+i+size)%size] = c
		}
		pos = (pos + l + skip + size) % size
		skip++
	}

	fmt.Println(list[0] * list[1])
}
