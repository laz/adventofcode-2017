package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type pair struct {
	x int
	y int
}

func (p pair) move(d int) pair {
	m := moves[d]
	return pair{p.x + m.x, p.y + m.y}
}

type node struct {
	pair
	ch byte
}

func (n node) String() string {
	return fmt.Sprintf("{%v %s}", n.pair, string(n.ch))
}

const (
	up = iota
	down
	left
	right
)

var moves = []pair{{0, -1}, {0, 1}, {-1, 0}, {1, 0}}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	grid := make([]string, 0, 201)
	path := make([]byte, 0, 26)
	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		grid = append(grid, scanner.Text())
	}

	current := node{pair{strings.Index(grid[0], "|"), 0}, '|'}
	direction := down
	end := false

	for !end {
		switch current.ch {
		case '|':
			new := current.pair.move(direction)
			current = node{new, grid[new.y][new.x]}
		case '-':
			new := current.pair.move(direction)
			current = node{new, grid[new.y][new.x]}
		case '+':
			if direction == up || direction == down {
				direction = left
				test := current.pair.move(direction)
				if grid[test.y][test.x] == ' ' {
					direction = right
					test = current.pair.move(direction)
				}
				current = node{test, grid[test.y][test.x]}
			} else if direction == left || direction == right {
				direction = up
				test := current.pair.move(direction)
				if grid[test.y][test.x] == ' ' {
					direction = down
					test = current.pair.move(direction)
				}
				current = node{test, grid[test.y][test.x]}
			}
		case ' ':
			panic(fmt.Sprintf("%s %i %s", current, direction, path))
		default:
			path = append(path, current.ch)
			new := current.pair.move(direction)
			current = node{new, grid[new.y][new.x]}
			end = current.ch == ' '
		}
	}
	fmt.Println(string(path))
}
