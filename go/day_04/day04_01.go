package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	valid := 0

	for scanner.Scan() {
		pass := true
		seen := map[string]bool{}
		words := strings.Split(scanner.Text(), " ")
		for _, word := range words {
			if _, ok := seen[word]; ok {
				pass = false
				break
			}
			seen[word] = true
		}
		if pass {
			valid++
		}
	}

	fmt.Println(valid)
}
