package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	valid := 0

	for scanner.Scan() {
		pass := true
		seen := map[string]bool{}
		words := strings.Split(scanner.Text(), " ")
		for _, word := range words {
			if _, ok := seen[word]; ok {
				pass = false
				break
			}
			permutations := perm([]rune(word), 0)
			for _, w := range permutations {
				seen[w] = true
			}
		}
		if pass {
			valid++
		}
	}

	fmt.Println(valid)
}

func perm(str []rune, i int) []string {
	res := []string{}
	if i == len(str) {
		res = append(res, string(str))
	} else {
		for j := i; j < len(str); j++ {
			str[i], str[j] = str[j], str[i]
			res = append(res, perm(str, i+1)...)
			str[i], str[j] = str[j], str[i]
		}
	}
	return res
}
