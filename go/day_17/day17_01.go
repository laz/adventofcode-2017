package main

import "fmt"

func main() {
	steps := 343

	max := 2017
	spinlock := make([]int, 2, max+1)
	spinlock[0] = 0
	spinlock[1] = 1

	cur := 1
	for i := cur; i < max; i++ {
		cur = (cur + steps) % len(spinlock)
		spinlock = append(spinlock[:cur+1], spinlock[cur:]...)
		cur++
		spinlock[cur] = i + 1
	}
	fmt.Println(spinlock[cur+1])
}
