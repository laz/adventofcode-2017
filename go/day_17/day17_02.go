package main

import "fmt"

func main() {
	steps := 343

	max := 50000000
	ans := 0
	cur := 1
	for i := cur; i < max; i++ {
		size := i + 1
		cur = ((cur + steps) % size) + 1
		if cur == 1 {
			ans = size
		}
	}
	fmt.Println(ans)
}
