package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	scanner.Scan()

	sum := 0
	input := []byte(scanner.Text())
	l := len(input)
	for i := 0; i < l; i++ {
		if input[i] == input[(i+1+l)%l] {
			sum += int(input[i]) - '0'
		}
	}

	fmt.Println(sum)
}
