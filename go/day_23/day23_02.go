package main

import (
	"fmt"
	"math/big"
)

func main() {
	count := 0
	for i := int64(108100); i <= 125100; i += 17 {
		if !big.NewInt(i).ProbablyPrime(1) {
			count++
		}
	}
	fmt.Println(count)
}
