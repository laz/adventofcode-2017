package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	registers := map[string]int{}
	for i := int('a'); i <= int('h'); i++ {
		registers[string(rune(i))] = 0
	}

	commands := make([][]string, 0, 25)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), " ")
		commands = append(commands, parts)
	}

	muls := 0
	for i := 0; i < len(commands); {
		line := commands[i]
		command := line[0]
		args := line[1:]

		switch command {
		case "set":
			if val, ok := registers[args[1]]; ok {
				registers[args[0]] = val
			} else {
				val, _ := strconv.Atoi(args[1])
				registers[args[0]] = val
			}
			i++
		case "jnz":
			move := 1
			parseit := false

			if val, ok := registers[args[0]]; ok {
				parseit = val != 0
			} else if val, err := strconv.Atoi(args[0]); err == nil {
				parseit = val != 0
			}

			if parseit {
				if d, ok := registers[args[1]]; ok {
					move = d
				} else if d, err := strconv.Atoi(args[1]); err == nil {
					move = d
				}
			}
			i += move
		case "sub":
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] -= val
			}
			i++
		case "mul":
			muls++
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] *= val
			}
			i++
		}
	}
	fmt.Println(muls)
}
