package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	reader := bufio.NewReader(r)

	total := 0
	depth := 0
	garbage := false
	for true {
		b, err := reader.ReadByte()
		if err != nil {
			break
		}

		switch b {
		case '{':
			if !garbage {
				depth++
			}
		case '}':
			if !garbage {
				total += depth
				if depth > 1 {
					depth--
				}
			}
		case '<':
			garbage = true
		case '>':
			if garbage {
				garbage = false
			}
		case '!':
			_, err := reader.ReadByte()
			if err != nil {
				break
			}
		}
	}

	fmt.Println(total)
}
