package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type point struct {
	x int
	y int
	z int
}

func (t *point) String() string {
	return fmt.Sprintf("{%d %d %d}", t.x, t.y, t.z)
}

func (t *point) add(a *point) {
	t.x += a.x
	t.y += a.y
	t.z += a.z
}

type particle struct {
	position     *point
	velocity     *point
	acceleration *point
	collided     bool
}

func (p *particle) String() string {
	return fmt.Sprintf("{%s %s %s}", p.position, p.velocity, p.acceleration)
}

func (p *particle) tick() {
	p.velocity.add(p.acceleration)
	p.position.add(p.velocity)
}

func (p *particle) distance() int {
	return abs(p.position.x) + abs(p.position.y) + abs(p.position.z)
}

func abs(i int) int {
	if i > 0 {
		return i
	}
	return -i
}

func newTrio(comDelim string) *point {
	parts := strings.Split(comDelim, ",")
	x, _ := strconv.Atoi(parts[0])
	y, _ := strconv.Atoi(parts[1])
	z, _ := strconv.Atoi(parts[2])
	return &point{x, y, z}
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	particles := make([]*particle, 0, 1000)

	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), ">, ")
		pos := strings.Split(parts[0], "=<")[1]
		vel := strings.Split(parts[1], "=<")[1]
		acc := strings.TrimSuffix(strings.Split(parts[2], "=<")[1], ">")

		par := &particle{
			newTrio(pos),
			newTrio(vel),
			newTrio(acc),
			false,
		}

		particles = append(particles, par)
	}

	for i := 0; i < 10000; i++ {
		occupied := map[point]*particle{}
		for _, p := range particles {
			p.tick()
			if o, ok := occupied[*p.position]; ok {
				p.collided = true
				o.collided = true
			} else {
				occupied[*p.position] = p
			}
		}

		keep := make([]*particle, 0, len(particles))
		for _, p := range particles {
			if !p.collided {
				keep = append(keep, p)
			}
		}
		particles = keep
	}

	fmt.Println(len(particles))
}
