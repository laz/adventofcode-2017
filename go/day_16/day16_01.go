package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	size := 16
	programs := make([]byte, size)
	for i := 0; i < size; i++ {
		programs[i] = byte(i) + 'a'
	}

	for scanner.Scan() {
		line := scanner.Text()
		for _, input := range strings.Split(line, ",") {
			switch input[0] {
			case 's':
				s, _ := strconv.Atoi(input[1:])
				s = size - (s % size)
				programs = append(programs[s:], programs[:s]...)
			case 'x':
				parts := strings.Split(input[1:], "/")
				x, _ := strconv.Atoi(parts[0])
				y, _ := strconv.Atoi(parts[1])
				programs[x], programs[y] = programs[y], programs[x]
			case 'p':
				parts := strings.Split(input[1:], "/")
				p := string(programs)
				x := strings.Index(p, parts[0])
				y := strings.Index(p, parts[1])
				programs[x], programs[y] = programs[y], programs[x]
			}
		}
	}

	fmt.Println(string(programs))
}
