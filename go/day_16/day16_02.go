package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	size := 16
	programs := initPrograms(size)

	seen := map[string]bool{}
	count := 1000000000
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), ",")
		i := 0
		for i < count {
			for _, input := range line {
				switch input[0] {
				case 's':
					s, _ := strconv.Atoi(input[1:])
					s = size - (s % size)
					programs = append(programs[s:], programs[:s]...)
				case 'x':
					parts := strings.Split(input[1:], "/")
					x, _ := strconv.Atoi(parts[0])
					y, _ := strconv.Atoi(parts[1])
					programs[x], programs[y] = programs[y], programs[x]
				case 'p':
					parts := strings.Split(input[1:], "/")
					p := string(programs)
					x := strings.Index(p, parts[0])
					y := strings.Index(p, parts[1])
					programs[x], programs[y] = programs[y], programs[x]
				}
			}
			if !seen[string(programs)] {
				seen[string(programs)] = true
				i++
			} else {
				seen = map[string]bool{}
				count = count % i
				i = 0
				programs = initPrograms(size)
			}
		}
	}

	fmt.Println(string(programs))
}

func initPrograms(size int) []byte {
	programs := make([]byte, size)
	for i := 0; i < size; i++ {
		programs[i] = byte(i) + 'a'
	}
	return programs
}
