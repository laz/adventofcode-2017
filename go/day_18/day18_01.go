package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	registers := map[string]int{}
	commands := make([][]string, 0, 25)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), " ")
		commands = append(commands, parts)
	}

	curSound := 0
	for i := 0; i < len(commands); {
		line := commands[i]
		command := line[0]
		args := line[1:]

		switch command {
		case "set":
			if val, ok := registers[args[1]]; ok {
				registers[args[0]] = val
			} else {
				val, _ := strconv.Atoi(args[1])
				registers[args[0]] = val
			}
			i++
		case "jgz":
			move := 1
			parseit := false

			val, ok := registers[args[0]]
			if ok {
				parseit = val > 0
			} else {
				parseit = args[0] > "0"
			}

			if parseit {
				if d, ok := registers[args[1]]; ok {
					move = d
				} else if d, err := strconv.Atoi(args[1]); err == nil {
					move = d
				}
			}
			i += move
		case "snd":
			val, ok := registers[args[0]]
			if !ok {
				val, _ = strconv.Atoi(args[0])
			}
			curSound = val
			i++
		case "rcv":
			val, ok := registers[args[0]]
			if !ok {
				val, _ = strconv.Atoi(args[0])
			}
			if val != 0 {
				i = len(commands)
			}
			i++
		case "add":
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] += val
			}
			i++
		case "mul":
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] *= val
			}
			i++
		case "mod":
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] %= val
			}
			i++
		}
	}
	fmt.Println(curSound)
}
