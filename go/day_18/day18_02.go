package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	commands := make([][]string, 0, 25)
	for scanner.Scan() {
		parts := strings.Split(scanner.Text(), " ")
		commands = append(commands, parts)
	}

	queueZero := make(chan int, 1000)
	queueOne := make(chan int, 1000)
	resultZero := make(chan int)
	resultOne := make(chan int)

	go execute(commands, map[string]int{"p": 0}, queueZero, queueOne, resultZero)
	go execute(commands, map[string]int{"p": 1}, queueOne, queueZero, resultOne)

	fmt.Println(<-resultOne)
}

func execute(commands [][]string, registers map[string]int, snd chan<- int, rcv <-chan int, results chan<- int) {
	sends := 0
	for i := 0; i < len(commands); {
		line := commands[i]
		command := line[0]
		args := line[1:]

		switch command {
		case "set":
			if val, ok := registers[args[1]]; ok {
				registers[args[0]] = val
			} else {
				val, _ := strconv.Atoi(args[1])
				registers[args[0]] = val
			}
			i++
		case "jgz":
			move := 1
			parseit := false

			val, ok := registers[args[0]]
			if ok {
				parseit = val > 0
			} else {
				parseit = args[0] > "0"
			}

			if parseit {
				if d, ok := registers[args[1]]; ok {
					move = d
				} else if d, err := strconv.Atoi(args[1]); err == nil {
					move = d
				}
			}
			i += move
		case "snd":
			val, ok := registers[args[0]]
			if !ok {
				val, _ = strconv.Atoi(args[0])
			}
			snd <- val
			sends++
			i++
		case "rcv":
			select {
			case val := <-rcv:
				registers[args[0]] = val
			case <-time.After(1 * time.Second):
				results <- sends
			}
			i++
		case "add":
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] += val
			}
			i++
		case "mul":
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] *= val
			}
			i++
		case "mod":
			_, ok := registers[args[0]]
			if ok {
				val, ok := registers[args[1]]
				if !ok {
					val, _ = strconv.Atoi(args[1])
				}
				registers[args[0]] %= val
			}
			i++
		}
	}
	results <- sends
}
