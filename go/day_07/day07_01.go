package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	top := []string{}
	parents := map[string]string{}
	weights := map[string]int{}
	for scanner.Scan() {
		input := scanner.Text()
		parts := strings.Split(input, " ")

		id := parts[0]
		weight, _ := strconv.Atoi(strings.Trim(parts[1], "()"))
		weights[id] = weight

		if len(parts) < 4 {
			top = append(top, id)
		} else {
			rest := parts[3:len(parts)]
			for i := 0; i < len(rest); i++ {
				child := strings.Trim(rest[i], ",")
				parents[child] = id
			}
		}
	}

	current := top
	for len(parents) > 0 {
		next := []string{}
		for _, n := range current {
			if p, ok := parents[n]; ok {
				next = append(next, p)
				delete(parents, n)
			}
		}
		current = next
	}

	fmt.Println(current)
}
