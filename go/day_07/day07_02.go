package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type node struct {
	parent      *node
	id          string
	weight      int
	totalWeight int
	children    []*node
}

func (n *node) total() int {
	n.totalWeight = n.weight
	for _, c := range n.children {
		n.totalWeight += c.totalWeight
	}
	return n.totalWeight
}

func (n *node) balanced() bool {
	if n.children != nil {
		sum := 0
		for _, c := range n.children {
			sum += c.weight
		}
		test := sum / len(n.children)
		for _, c := range n.children {
			if c.weight != test {
				return false
			}
		}
	}
	return true
}

func (n *node) String() string {
	out := &bytes.Buffer{}
	pid := "-1"
	if n.parent != nil {
		pid = n.parent.id
	}
	childCount := 0
	if n.children != nil {
		childCount = len(n.children)
	}
	out.WriteString(fmt.Sprintf("[%s %d %d %s %d]", n.id, n.weight, n.totalWeight, pid, childCount))
	return out.String()
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	nodes := map[string]*node{}

	for scanner.Scan() {
		input := scanner.Text()
		parts := strings.Split(input, " ")

		id := parts[0]
		weight, _ := strconv.Atoi(strings.Trim(parts[1], "()"))
		n, ok := nodes[id]
		if ok {
			n.weight = weight
		} else {
			n = &node{id: id, weight: weight}
			nodes[id] = n
		}

		if len(parts) > 3 {
			rest := parts[3:len(parts)]
			children := make([]*node, len(rest))

			for i := 0; i < len(rest); i++ {
				cid := strings.Trim(rest[i], ",")

				child, ok := nodes[cid]
				if ok {
					child.parent = n
				} else {
					child = &node{id: cid, parent: n}
					nodes[cid] = child
				}

				children[i] = child
			}

			n.children = children
		}
	}

	root := &node{}
	for _, n := range nodes {
		if n.parent == nil {
			root = n
			break
		}
	}

	totalWeights(root)

	u := findUnbalanced(root)
	for _, s := range u.parent.children {
		if s.id != u.id {
			u.weight += s.totalWeight - u.totalWeight
			break
		}
	}
	u.total()
	fmt.Println(u)
	fmt.Println(u.parent.children)

	totalWeights(root)
	fmt.Println(root, root.children)
}

func totalWeights(n *node) {
	if n.children != nil {
		for _, c := range n.children {
			totalWeights(c)
		}
	}
	n.total()
}

func findUnbalanced(n *node) *node {
	b := n
	if !n.balanced() {
		target := 0
		for _, c := range n.children {
			if c.totalWeight == target {
				break
			}
			target = c.totalWeight
		}
		next := &node{}
		for _, c := range n.children {
			if c.totalWeight != target {
				next = c
				break
			}
		}
		if next.id != "" {
			b = findUnbalanced(next)
		}
	}
	return b
}

func printTree(n *node) {
	fmt.Println()
	if n.children != nil {
		for _, c := range n.children {
			printTree(c)
		}
		fmt.Println()
	}
	fmt.Print(n)
}
