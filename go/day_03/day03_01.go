package main

import "fmt"

func main() {
	start := 289326
	size := 0
	for i := 1; i < start/2; i += 2 {
		if i*i > start {
			size = i
			break
		}
	}

	br := size * size
	bl := br - size + 1
	tl := bl - size + 1
	tr := tl - size + 1
	fl := tr - size + 2

	fmt.Println(br, " ", bl, " ", tl, " ", tr, " ", fl)

	center := []int{(size) / 2, (size) / 2}
	pos := make([]int, 2)
	if start >= bl {
		pos[0] = size - 1 - (br - start)
		pos[1] = size - 1
	} else if start < bl && start >= tl {
		pos[0] = 0
		pos[1] = size - 1 - (bl - start)
	} else if start < tl && start >= tr {
		pos[0] = tl - start
		pos[1] = 0
	} else if start < tr && start >= fl {
		pos[0] = size - 1
		pos[1] = (br - start)
	}

	fmt.Println(abs(center[0]-pos[0]) + abs(center[1]-pos[1]))

}

func abs(i int) int {
	if i < 0 {
		return -i
	}
	return i
}
