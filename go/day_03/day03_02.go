package main

import (
	"bytes"
	"fmt"
)

const (
	right = iota
	up
	down
	left
)

type pair [2]int
type matrix [][]int

func (m *matrix) String() string {
	b := bytes.Buffer{}
	for _, c := range *m {
		for _, r := range c {
			b.WriteString(fmt.Sprintf("  %06d", r))
		}
		b.WriteString("\n\n")
	}
	return b.String()
}

var moves = []pair{{0, 1}, {-1, 0}, {0, -1}, {1, 0}}

var adjacent = []pair{{0, 1}, {-1, 1}, {-1, 0}, {-1, -1}, {0, -1}, {1, -1}, {1, 0}, {1, 1}}

func main() {
	target := 289326
	size := 11
	grid := matrix(make([][]int, size))
	for i := range grid {
		grid[i] = make([]int, size)
	}

	n := 0
	loc := pair{size / 2, size / 2}
	grid[loc[0]][loc[1]] = 1
	dir := 0
	for i := 1; n < target; i++ {
		loc, dir = move(loc, &grid, dir)
		fmt.Println(loc, " ", dir, " ", n)
		fmt.Println(&grid)

		grid[loc[0]][loc[1]] = sum(loc, &grid)
		n = grid[loc[0]][loc[1]]
	}
	fmt.Println(loc, " ", dir, " ", n)
	fmt.Println(&grid)
}

func move(p pair, mx *matrix, d int) (pair, int) {
	mv := moves[d%4]
	m := [][]int(*mx)
	t := pair{p[0] + mv[0], p[1] + mv[1]}
	if t[0] >= 0 && t[0] < len(m) && t[1] >= 0 && t[1] < len(m) {
		if m[t[0]][t[1]] == 0 {
			return t, (d + 1) % 4
		}
	}
	return move(p, mx, (d+3)%4)
}

func sum(p pair, mx *matrix) int {
	sum := 0
	m := [][]int(*mx)
	for _, a := range adjacent {
		t := pair{p[0] - a[0], p[1] - a[1]}
		if t[0] >= 0 && t[0] < len(m) && t[1] >= 0 && t[1] < len(m) {
			sum += m[t[0]][t[1]]
		}
	}
	return sum
}
