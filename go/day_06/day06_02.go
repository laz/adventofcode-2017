package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type bankSet []int

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	input := []string{}
	for scanner.Scan() {
		input = append(input, scanner.Text())
	}

	numBanks := len(input)
	banks := bankSet(make([]int, numBanks))
	for i, r := range input {
		j, _ := strconv.Atoi(r)
		banks[i] = j
	}

	seen := map[string]int{}

	cycles := 0
	prev, found := seen[banks.String()]
	for !found {
		seen[banks.String()] = cycles

		max := banks.max()
		val := banks[max]

		next := bankSet(make([]int, numBanks))
		banks[max] = 0

		copy(next, banks)
		for i := max + 1; val > 0; i++ {
			next[i%numBanks]++
			val--
		}

		banks = next
		cycles++
		prev, found = seen[banks.String()]
	}

	fmt.Println(cycles - prev)
}

func (s bankSet) max() int {
	m := 0
	for i := range s {
		if s[i] > s[m] {
			m = i
		}
	}
	return m
}

func (s bankSet) String() string {
	return fmt.Sprint([]int(s))
}
