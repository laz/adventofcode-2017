package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type component struct {
	portA, portB int
	connA, connB bool
	next         *component
}

func (c *component) connect(t *component) bool {
	return c.connectOrCompat(t, true)
}

func (c *component) compatible(t *component) bool {
	return c.connectOrCompat(t, false)
}

func (c *component) connectOrCompat(t *component, fuse bool) bool {
	result := false
	if c.portA == t.portA && !c.connA && !t.connA {
		if fuse {
			c.connA = true
			t.connA = true
		}
		result = true
	} else if c.portB == t.portB && !c.connB && !t.connB {
		if fuse {
			c.connB = true
			t.connB = true
		}
		result = true
	} else if c.portA == t.portB && !c.connA && !t.connB {
		if fuse {
			c.connA = true
			t.connB = true
		}
		result = true
	} else if c.portB == t.portA && !c.connB && !t.connA {
		if fuse {
			c.connB = true
			t.connA = true
		}
		result = true
	}
	if result && fuse {
		c.next = t
	}

	return result
}

func (c *component) clone() *component {
	res := &component{}
	res.portA = c.portA
	res.connA = c.connA
	res.portB = c.portB
	res.connB = c.connB
	p := c.next
	if p != nil {
		res.next = p.clone()
	}
	return res
}

func (c *component) sum() (int, int) {
	sum := c.portA + c.portB
	n := 1
	p := c.next
	for p != nil {
		sum += p.portA + p.portB
		p = p.next
		n++
	}
	return sum, n
}

type bridge struct {
	start *component
}

func (b *bridge) end() *component {
	c := b.start
	for c.next != nil {
		c = c.next
	}
	return c
}

func main() {
	r := os.Stdin
	if len(os.Args) > 1 {
		if f, err := os.Open(os.Args[1]); err == nil {
			r = f
			defer r.Close()
		} else {
			panic(err)
		}
	}

	scanner := bufio.NewScanner(r)

	input := make([]string, 0, 55)
	for scanner.Scan() {
		input = append(input, scanner.Text())
	}

	components := make([]*component, len(input))
	for i, c := range input {
		parts := strings.Split(c, "/")
		a, _ := strconv.Atoi(parts[0])
		b, _ := strconv.Atoi(parts[1])
		components[i] = &component{portA: a, portB: b}
	}

	keep := make([]*component, 0, len(components))
	start := make([]*bridge, 0, len(components)/2)
	for _, c := range components {
		if c.portA == 0 || c.portB == 0 {
			c := c.clone()
			if c.portA == 0 {
				c.connA = true
			} else if c.portB == 0 {
				c.connB = true
			}

			br := &bridge{c}
			start = append(start, br)
		} else {
			keep = append(keep, c)
		}
	}

	components = keep
	bridges := []*bridge{}
	for _, br := range start {
		bridges = append(bridges, build(br, components)...)
	}
	maxStrengths := map[int]int{}
	maxLength := 0
	for _, br := range bridges {
		sum, length := br.start.sum()
		if length > maxLength {
			maxLength = length
		}
		if sum > maxStrengths[length] {
			maxStrengths[length] = sum
		}
		p := br.start
		for p != nil {
			p = p.next
		}
	}
	fmt.Println(maxStrengths[maxLength])
}

func build(br *bridge, free []*component) []*bridge {
	cand, rest := candidates(br.end(), free)
	if len(cand) == 0 {
		return []*bridge{br}
	}

	result := []*bridge{}
	for i, test := range cand {
		t := test.clone()
		n := &bridge{br.start.clone()}
		n.end().connect(t)

		c := make([]*component, len(cand))
		copy(c, cand)

		r := make([]*component, len(rest))
		copy(r, rest)
		c = append(c[:i], c[i+1:]...)
		r = append(r, c...)

		more := build(n, r)
		result = append(result, more...)
	}
	return result
}

func candidates(c *component, d []*component) ([]*component, []*component) {
	results := make([]*component, 0, len(d))
	rest := make([]*component, 0, len(d))
	for _, t := range d {
		if c.compatible(t) {
			results = append(results, t)
		} else {
			rest = append(rest, t)
		}
	}
	return results, rest
}
